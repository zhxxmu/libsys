<?php

/**
* @file
* Contains \Drupal\libsys\Form\SettingsForm.
*/

namespace Drupal\libsys\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
* Configure Database Source of libsys
*/
class SettingsForm extends ConfigFormBase {
	/**
	* {@inheritdoc}
	*/
	public function getFormId() {
		return 'libsys_settings';
	}
	
	/**
	* {@inheritdoc}
	*/
	protected function getEditableConfigNames() {
		return ['libsys.datasource', 'libsys.qyweixin'];
	}
	
	/**
	* {@inheritdoc}
	*/
	public function buildForm(array $form, FormStateInterface $form_state) {
		$datasource = $this->config('libsys.datasource');
		$form=[];
		$form['datasource']=array(
			'#type'=>'details',
			'#title'=>$this->t('Data source settings'),
			'#open'=>TRUE,
		);
		$form['datasource']['host']=array(
			'#type' => 'textfield',
			'#title' => $this->t('Host'),
			'#default_value' => empty($datasource->get('host'))?'':$datasource->get('host'),
			'#required' => TRUE,
		);
		$form['datasource']['port']=array(
			'#type' => 'number',
			'#min' => 1,
			'#title' => $this->t('Port'),
			'#default_value' => empty($datasource->get('port'))?'1521':$datasource->get('port'),
			'#required' => TRUE,
		);
		$form['datasource']['database']=array(
			'#type' => 'textfield',
			'#title' => $this->t('Database Name'),
			'#default_value' => empty($datasource->get('database'))?'':$datasource->get('database'),
			'#required' => TRUE,
		);
		$form['datasource']['username']=array(
			'#type' => 'textfield',
			'#title' => $this->t('Username'),
			'#default_value' => empty($datasource->get('username'))?'':$datasource->get('username'),
			'#required' => TRUE,
		);
		$form['datasource']['password']=array(
			'#type' => 'textfield',
			'#title' => $this->t('Password'),
			'#description' => $this->t('Please note that, the password will be stored unencrypted as we need to run this interface unattended.'),
			'#default_value' => empty($datasource->get('password'))?'':$datasource->get('password'),
			'#required' => TRUE,
		);
		
		$should_show_qyweixin=\Drupal::moduleHandler()->moduleExists('qyweixin');
		$qyweixin_config=$this->config('libsys.qyweixin');
		$form['qyweixin']=array(
			'#type'=>'details',
			'#title'=>$this->t('Qiye Weixin settings'),
			'#open'=>TRUE,
			'#tree'=>TRUE,
			'#access'=>$should_show_qyweixin,
		);
		$form['qyweixin']['notify_overdue']=array(
			'#type' => 'checkbox',
			'#title' => $this->t('Notify reader if the book he borrowed is overdued, once per day.'),
			'#default_value' => empty($qyweixin_config->get('notify_overdue'))?'':$qyweixin_config->get('notify_overdue'),
		);
		$form['qyweixin']['notify_upcoming']=array(
			'#type' => 'checkbox',
			'#title' => $this->t('Notify reader if the book he borrowed is expected to overduded.'),
			'#default_value' => empty($qyweixin_config->get('notify_upcoming'))?'':$qyweixin_config->get('notify_upcoming'),
		);
		$form['qyweixin']['notify_upcoming_days']=array(
			'#type' => 'textfield',
			'#title_display' => 'none',
			'#field_prefix' => $this->t('&nbsp;before:'),
			'#field_suffix'=>$this->t('days.'),
			'#size' => 10,
			'#title' => $this->t('Days before overdue'),
			'#description' => $this->t('like: 1, 3, 6'),
			'#default_value' => empty($qyweixin_config->get('notify_upcoming_days'))?'':$qyweixin_config->get('notify_upcoming_days'),
			'#states' => array(
				'visible' => array(
					':input[name="qyweixin[notify_upcoming]"]' => ['checked' => TRUE],
				),
				'required' => array(
					':input[name="qyweixin[notify_upcoming]"]' => ['checked' => TRUE],
				),
			),
		);

		$form['qyweixin']['notify_preg_arri']=array(
			'#type' => 'checkbox',
			'#title' => $this->t('Notify reader if the book he preserved has been arrived.'),
			'#default_value' => empty($qyweixin_config->get('notify_preg_arri'))?'':$qyweixin_config->get('notify_preg_arri'),
		);
		$form['qyweixin']['notify_notice'] = array(
			'#type' => 'markup',
			'#markup' => $this->t('Please note that there might be huge data queued to send, which cannot fulfill in single one cron job, you\'ll need to add another queue-run command after cron in drush, if running via drush.'),
		);

                $form['actions']['syncreader'] = [
                        '#access' => !empty($datasource) && \Drupal::moduleHandler()->moduleExists('qyweixin'),
                        '#type'=>'submit',
                        '#value'=>$this->t('Sync Reader binding from Qyweixin'),
                        '#button_type' => 'secondary',
                        '#weight' => 10,
                        '#submit' => ['::SyncReaderSubmit'],
                ];
		
		return parent::buildForm($form, $form_state);
	}

	/**
	* {@inheritdoc}
	*/
	public function validateForm(array &$form, FormStateInterface $form_state) {
		if(!filter_var($form_state->getValue('host'), FILTER_VALIDATE_IP)) {
			$form_state->setErrorByName('host', $this->t('Invalid IP Address.'));
			return;
		}
		
		try {
			$conn=@oci_connect($form_state->getValue('username'),$form_state->getValue('password'),
				sprintf("%s:%s/%s", $form_state->getValue('host'), $form_state->getValue('port'), $form_state->getValue('database')), 'AL32UTF8'
			);
			if(!$conn) {
				$e=oci_error();
				throw new \Exception($e['message'], $e['code']);
			}
			
			$sql='select * from product_component_version';
			$stmt = @oci_parse($conn, $sql);
			if(!$stmt) {
				$e=oci_error($stmt);
				throw new \Exception($e['message'], $e['code']);
			}

			$r=@oci_execute($stmt, OCI_DEFAULT);
			if(!$r) {
				$e=oci_error($stmt);
				throw new \Exception($e['message'], $e['code']);
			}
		} catch (\Exception $e) {
			switch($e->getCode()) {
				case '1017':
					$form_state->setErrorByName('username', $e->getCode().': '.$e->getMessage());
					$form_state->setErrorByName('password', $e->getCode().': '.$e->getMessage());
					break;
				default:
					$form_state->setErrorByName('host', $e->getCode().': '.$e->getMessage());
			}
		} finally {
			if(!empty($stmt))
				oci_free_statement($stmt);
			if(!empty($conn))
				oci_close($conn);
		}
		
		if(!empty($form_state->getValue(['qyweixin','notify_upcoming_days']))) {
			$days=explode(',', $form_state->getValue(['qyweixin','notify_upcoming_days']));
			foreach($days as $d) {
				if(!preg_match('/^[0-9]+$/',trim($d))) {
					$form_state->setErrorByName('qyweixin][notify_upcoming_days', $this->t('The value should be in the form of "1, 3, 6"'));
				}
			}
		}
	}

	/**
	* {@inheritdoc}
	*/
	public function submitForm(array &$form, FormStateInterface $form_state) {
		$this->config('libsys.datasource')
			->set('host', $form_state->getValue('host'))
			->set('port', $form_state->getValue('port'))
			->set('database', $form_state->getValue('database'))
			->set('username', $form_state->getValue('username'))
			->set('password', $form_state->getValue('password'))
			->save();
		
		if($form_state->getValue('qyweixin'))
			$this->config('libsys.qyweixin')
				->set('notify_overdue', $form_state->getValue(['qyweixin', 'notify_overdue']))
				->set('notify_upcoming', $form_state->getValue(['qyweixin', 'notify_upcoming']))
				->set('notify_upcoming_days', $form_state->getValue(['qyweixin', 'notify_upcoming_days']))
				->set('notify_preg_arri', $form_state->getValue(['qyweixin', 'notify_preg_arri']))
				->save();

		parent::submitForm($form, $form_state);
	}

	public function SyncReaderSubmit(array &$form, FormStateInterface $form_state) {
		$batch=\libsys_sync_reader_binding_batch_build();
		batch_set($batch);
	}
}
?>

