<?php

/**
* @file
* Contains \Drupal\libsys\Form\SmartIBusDataSourceForm.
*/

namespace Drupal\libsys\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
* Configure Database Source of libsys
*/
class SmartIBusDataSourceForm extends ConfigFormBase {
	/**
	* {@inheritdoc}
	*/
	public function getFormId() {
		return 'libsys_smartibus_databases_settings';
	}
	
	/**
	* {@inheritdoc}
	*/
	protected function getEditableConfigNames() {
		return ['libsys.smartibus.datasource'];
	}
	
	/**
	* {@inheritdoc}
	*/
	public function buildForm(array $form, FormStateInterface $form_state) {
		$datasource = $this->config('libsys.smartibus.datasource');
		$form=[];
		$form['host']=array(
			'#type' => 'textfield',
			'#title' => $this->t('Host'),
			'#default_value' => empty($datasource->get('host'))?'':$datasource->get('host'),
			'#required' => TRUE,
		);
		$form['port']=array(
			'#type' => 'number',
			'#min' => 1,
			'#title' => $this->t('Port'),
			'#default_value' => empty($datasource->get('port'))?'1521':$datasource->get('port'),
			'#required' => TRUE,
		);
		$form['database']=array(
			'#type' => 'textfield',
			'#title' => $this->t('Database Name'),
			'#default_value' => empty($datasource->get('database'))?'':$datasource->get('database'),
			'#required' => TRUE,
		);
		$form['username']=array(
			'#type' => 'textfield',
			'#title' => $this->t('Username'),
			'#default_value' => empty($datasource->get('username'))?'':$datasource->get('username'),
			'#required' => TRUE,
		);
		$form['password']=array(
			'#type' => 'textfield',
			'#title' => $this->t('Password'),
			'#description' => $this->t('Please note that, the password will be stored unencrypted as we need to run this interface unattended.'),
			'#default_value' => empty($datasource->get('password'))?'':$datasource->get('password'),
			'#required' => TRUE,
		);

		$form['actions']['importphoto'] = [
			'#access' => !empty($datasource),
			'#type'=>'submit',
			'#value'=>$this->t('Clear and Import photos'),
			'#button_type' => 'secondary',
			'#weight' => 10,
			'#submit' => ['::importPhotoSubmit'],
		];

		return parent::buildForm($form, $form_state);
	}

	/**
	* {@inheritdoc}
	*/
	public function validateForm(array &$form, FormStateInterface $form_state) {
		try {
			$conn=@oci_connect($form_state->getValue('username'),$form_state->getValue('password'),
				sprintf("%s:%s/%s", $form_state->getValue('host'), $form_state->getValue('port'), $form_state->getValue('database')), 'AL32UTF8'
			);
			if(!$conn) {
				$e=oci_error();
				throw new \Exception($e['message'], $e['code']);
			}
			
			$sql='select * from product_component_version';
			$stmt = @oci_parse($conn, $sql);
			if(!$stmt) {
				$e=oci_error($stmt);
				throw new \Exception($e['message'], $e['code']);
			}

			$r=@oci_execute($stmt, OCI_DEFAULT);
			if(!$r) {
				$e=oci_error($stmt);
				throw new \Exception($e['message'], $e['code']);
			}
		} catch (\Exception $e) {
			$form_state->setErrorByName('host', $e->code.': '.$e->getMessage());
		} finally {
			if(!empty($stmt))
				oci_free_statement($stmt);
			if(!empty($conn))
				oci_close($conn);
		}
	}

	/**
	* {@inheritdoc}
	*/
	public function submitForm(array &$form, FormStateInterface $form_state) {
		$this->config('libsys.smartibus.datasource')
			->set('host', $form_state->getValue('host'))
			->set('port', $form_state->getValue('port'))
			->set('database', $form_state->getValue('database'))
			->set('username', $form_state->getValue('username'))
			->set('password', $form_state->getValue('password'))
			->save();

		parent::submitForm($form, $form_state);
	}

	public function importPhotoSubmit(array &$form, FormStateInterface $form_state) {
		$batch=\libsys_import_photo_batch_build();
		batch_set($batch);
	}
}
?>

