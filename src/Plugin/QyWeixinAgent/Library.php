<?php

/**
 * @file
 * Contains \Drupal\libsys\Plugin\QyWeixinAgent\Library.
 */

namespace Drupal\libsys\Plugin\QyWeixinAgent;

use Drupal\qyweixin\CorpBase;
use Drupal\qyweixin\AgentBase;
use Drupal\qyweixin\Annotation\QyWeixinAgent;
use Drupal\Core\Form\FormStateInterface;


/**
 * Libsys interface from qyweixin.
 *
 * @QyWeixinAgent(
 *   id = "library",
 * )
 */
class Library extends AgentBase {

	/**
	* {@inheritdoc}
	*/
	public function getName() {
		return 'Library plugin test';
	}
	
	public function defaultResponse($message) {
		return 'OK';
	}
	
	public function textResponse($message) {
		return 'Received '.$message->Content;
	}

}
?>

