<?php

/**
 * @file
 * Contains \Drupal\gdlisnet\Plugin\QueueWorker\NotifyOverdue.
 */

namespace Drupal\libsys\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\qyweixin\CorpBase;
use Drupal\qyweixin\MessageBase;

/**
 * Notify Readers that are overdued.
 *
 * @QueueWorker(
 *   id = "libsys.overdue",
 *   cron = {"time" = 60}
 * )
 */
class NotifyOverdue extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($reader_info) {
        $user=$reader_info->readerid;
        $data=$reader_info->data;
        if(!is_array($data)) $data=[$data];

        if(empty($data)) return;

        try {
                $user=CorpBase::userConvertToUserid($user);
                $p=\Drupal::service('plugin.manager.qyweixin.agent')->createInstance('library');
                foreach($data as $d) {
                        $msg=new MessageBase();
                        $msg->setMsgType(MessageBase::MESSAGE_TYPE_TEXT)->setContent((string)$d)->setToUser($user);
                        $p->messageSend($msg);
                }
                \Drupal::logger('libsys')->debug(t('Message to @user sent.',['@user'=>$user]));
        } catch (\Exception $e) {
                \Drupal::logger('libsys')->error(t('Message to @user error: @msg',['@user'=>$user, '@msg'=>$e->getMessage()]));
        }
  }
}
?>
