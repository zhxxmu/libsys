<?php

/**
 * @file
 * Contains \Drupal\libsys\Plugin\QueueWorker\DIsablePeople.
 */

namespace Drupal\libsys\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Disable certian reader.
 *
 * @QueueWorker(
 *   id = "libsys_disable_reader",
 *   title = @Translation("Disable certain reader"),
 *   cron = {"time" = 60}
 * )
 */
class DisablePeople extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($reader_info) {
	$reader=$reader_info[0];
	$status=$reader_info[1];
	// First let's fetch all the database settings
	$libsys_database = \Drupal::config('libsys.datasource');
	
	try {
		// Use php-oci8 rather than pdo_oci to deal with blob data, which is faster
		$conn=oci_connect($libsys_database->get('username'), $libsys_database->get('password'),
			sprintf("%s:%s/%s", $libsys_database->get('host'), $libsys_database->get('port'), $libsys_database->get('database')), 'AL32UTF8'
		);
		if(!$conn) {
			$e=oci_error();
			throw new \Exception($e['message']);
		}
		
		if(!empty($reader)) {
			// Get CERT_ID from barcode
			$sql=sprintf("UPDATE READER SET REDR_FLAG='%s', REDR_DEL_DAY='%s' WHERE CERT_ID='%s' AND REMARK='CARD'", $status, $reader->LXSJ, $reader->ZJHM);
			$stmt = oci_parse($conn, $sql);
			if(!$stmt) {
				$e=oci_error($stmt);
				throw new \Exception($e['message']);
			}
			$r=oci_execute($stmt, OCI_DEFAULT);
			if(!$r) {
				$e=oci_error($stmt);
				throw new \Exception($e['message']);
			}
		}
	} catch (\Exception $e) {
		if(!empty($stmt))
			oci_free_statement($stmt);
		if(!empty($conn))
			oci_close($conn);
		throw new \Exception($e->getMessage());
	} finally {
		oci_commit($conn);
		if(!empty($stmt))
			oci_free_statement($stmt);
		if(!empty($conn))
			oci_close($conn);
	}
  }
}
?>
